//
//  MenuItem.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 21/02/21.
//

import Foundation
class MenuItem{
    var name: String
    var price: Float
    var image: String
    var quantity: Int
    init(name: String, price: Float, image: String, quantity: Int) {
        self.name = name
        self.price = price
        self.image = image
        self.quantity = quantity
    }
    
}
