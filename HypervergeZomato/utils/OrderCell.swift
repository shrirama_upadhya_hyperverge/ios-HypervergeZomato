//
//  OrderCell.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 22/02/21.
//

import UIKit

class OrderCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var orderRestImage: UIImageView!
    @IBOutlet weak var orderRestNameLabel: UILabel!
    @IBOutlet weak var orderDateLabel: UILabel!
    @IBOutlet weak var orderDetailsLabel: UILabel!
    @IBOutlet weak var orderTotalPriceLabel: UILabel!
    func configure(orderDetails: OrderHistoryCard){
        cardView.clipsToBounds = true
        cardView.layer.shadowColor = UIColor.gray.cgColor
        cardView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        cardView.layer.shadowOpacity = 1.0
        cardView.layer.masksToBounds = false
        cardView.layer.cornerRadius = 5
        let imageURL = URL(string: orderDetails.orderRestImage)!
        let session = URLSession(configuration: .default)
        DispatchQueue.global(qos: .background).async{
            let downloadPicTask = session.dataTask(with: imageURL) { (data, response, error) in
                if let e = error {
                    print("Error downloading cat picture: \(e)")
                } else {
                    if let res = response as? HTTPURLResponse {
                        print("Downloaded restaurant picture with response code \(res.statusCode)")
                        if let imageData = data {
                            DispatchQueue.main.async{
                                self.orderRestImage.image = UIImage(data: imageData)
                            }
                        } else {
                            print("Couldn't get image: Image is nil")
                        }
                    } else {
                        print("Couldn't get response code for some reason")
                    }
                }
            }
            
            downloadPicTask.resume()
        }
        self.orderDateLabel.text = "Ordered On : \(orderDetails.orderDate)"
        self.orderRestNameLabel.text = "\(orderDetails.orderRestName), \(orderDetails.orderRestLocation)"
        
        var itemDetailsString: String = ""
        var totalPrice: Float = 0.0
        for item in orderDetails.orderItems{
            itemDetailsString.append("\(item.name)(\(item.quantity))")
            totalPrice = totalPrice + item.price
        }
        
        self.orderDetailsLabel.text = itemDetailsString
        let s = NSString(format: "%.2f", totalPrice)
        self.orderTotalPriceLabel.text = "\u{20B9} " + (s as String)
        
    }
    
}
