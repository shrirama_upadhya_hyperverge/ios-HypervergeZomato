//
//  CardCell.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 20/02/21.
//

import UIKit

class RestaurantCardCell: UITableViewCell {
    
    @IBOutlet var cardView: UIView!
    @IBOutlet var pictureView: UIImageView!
    @IBOutlet var restName: UILabel!
    @IBOutlet var restCuisine: UILabel!
    @IBOutlet weak var restRating: UILabel!

    func configure(picture: String, restaurantName: String, restaurantCuisine : String, restaurantRating: Float){
        let imageURL = URL(string: picture)!
        let session = URLSession(configuration: .default)
        DispatchQueue.global(qos: .background).async {
            let downloadPicTask = session.dataTask(with: imageURL) { (data, response, error) in
                if let e = error {
                    print("Error downloading cat picture: \(e)")
                } else {
                    if let res = response as? HTTPURLResponse {
                        print("Downloaded restaurant picture with response code \(res.statusCode)")
                        if let imageData = data {
                            DispatchQueue.main.async{
                                self.pictureView.image = UIImage(data: imageData)
                            }
                        } else {
                            print("Couldn't get image: Image is nil")
                        }
                    } else {
                        print("Couldn't get response code for some reason")
                    }
                }
            }

            downloadPicTask.resume()
                }
        
        restName.text = restaurantName
        restCuisine.text = restaurantCuisine
        restRating.text = "\u{2606} \(restaurantRating)"
        cardView.clipsToBounds = true
        pictureView.clipsToBounds = true
        pictureView.layer.cornerRadius = 10
        cardView.layer.shadowColor = UIColor.gray.cgColor
        cardView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        cardView.layer.shadowOpacity = 1.0
        cardView.layer.masksToBounds = false
        cardView.layer.cornerRadius = 10
        
    }
}
