//
//  myCellTableViewCell.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 20/02/21.
//

import UIKit

class RestaurantMenuCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var menuItemImage: UIImageView!
    @IBOutlet weak var menuItemAddButton: UIButton!
    @IBOutlet weak var menuItemName: UILabel!
    @IBOutlet weak var menuItemPrice: UILabel!
    
    weak var cellDelegate: TVCellDelegate?
    var indexPath: IndexPath!
    
    var menuItem: MenuItem?
    var itemPriceString: String?
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.cellDelegate = nil
    }
    
    func configureItem(menuItem: MenuItem){
        self.menuItem = menuItem
        
        let s = NSString(format: "%.2f", self.menuItem!.price)
        self.itemPriceString = s as String
        
        menuItemImage.image = UIImage(named: self.menuItem!.image)
        menuItemName.text = self.menuItem?.name
        menuItemPrice.text = "\u{20B9}"+(self.itemPriceString!)
        menuItemAddButton.addTarget(self, action: #selector(addItemToList) , for: .touchUpInside)
        
        
        cardView.clipsToBounds = true
        menuItemImage.clipsToBounds = true
        menuItemImage.layer.cornerRadius = 5
        cardView.layer.shadowColor = UIColor.gray.cgColor
        cardView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        cardView.layer.shadowOpacity = 1.0
        cardView.layer.masksToBounds = false
        cardView.layer.cornerRadius = 5
        
    }
    
    @objc func addItemToList(){
        self.cellDelegate?.didPressAddButton(indexPath: self.indexPath)
    }
}
protocol TVCellDelegate : class {
    func didPressAddButton(indexPath: IndexPath)
}
