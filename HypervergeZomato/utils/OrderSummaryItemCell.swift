//
//  OrderSummaryItemCell.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 21/02/21.
//

import UIKit

class OrderSummaryItemCell: UITableViewCell {

    @IBOutlet weak var labelOrderItemName: UILabel!
    @IBOutlet weak var labelOrderItemPrice: UILabel!
    
    var orderItem: MenuItem!
    func configure(orderItem: MenuItem){
        let s = NSString(format: "%.2f", (orderItem.price * (Float(orderItem.quantity))))
        labelOrderItemName.text = "\(orderItem.name) : \(orderItem.quantity)"
        labelOrderItemPrice.text = "\u{20B9} \(s)"
    }

}
