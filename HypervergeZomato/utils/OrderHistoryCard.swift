//
//  OrderHistoryCard.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 22/02/21.
//

import Foundation

class OrderHistoryCard{
    var orderID : Int
    var orderRestName : String
    var orderRestLocation : String
    var orderDate : String
    var orderItems : [MenuItem]
    var orderRestImage : String
    
    init(orderID: Int, orderRestName: String, orderRestLocation : String, orderDate : String, orderItems : [MenuItem], orderRestImage: String) {
        self.orderID = orderID
        self.orderDate = orderDate
        self.orderRestName = orderRestName
        self.orderRestLocation = orderRestLocation
        self.orderItems = orderItems
        self.orderRestImage = orderRestImage
    }
    
}
