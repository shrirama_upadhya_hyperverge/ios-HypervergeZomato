//
//  Restaurant.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 20/02/21.
//

import Foundation

class RestCard{
    var name, cuisine: String
    var image: String
    var rating: Float
    init(name: String, cuisine: String, image: String, rating: Float) {
        self.name = name
        self.cuisine = cuisine
        self.image = image
        self.rating = rating
    }
}
