//
//  ButtonWithImage.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 22/02/21.
//

import UIKit

class ButtonWithImage: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func layoutSubviews() {
            super.layoutSubviews()
            if imageView != nil {
                imageEdgeInsets = UIEdgeInsets(top: 5, left: (bounds.width - 35), bottom: 5, right: 5)
                titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)!)
            }
        }

}
