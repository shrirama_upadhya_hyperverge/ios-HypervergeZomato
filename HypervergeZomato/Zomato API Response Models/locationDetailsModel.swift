////
////  locationDetailsModel.swift
////  HypervergeZomato
////
////  Created by Shrirama Upadhya A on 19/02/21.
////
//
//// This file was generated from JSON Schema using quicktype, do not modify it directly.
//// To parse the JSON, add this file to your project and do:
////
////   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.welcomeTask(with: url) { welcome, response, error in
////     if let welcome = welcome {
////       ...
////     }
////   }
////   task.resume()
//
//import Foundation
//
//// MARK: - Welcome
//struct Welcome: Codable {
//    let popularity, nightlifeIndex: String
//    let nearbyRes, topCuisines: [String]
//    let popularityRes, nightlifeRes, subzone: String
//    let subzoneID: Int
//    let city: String
//    let location: WelcomeLocation
//    let numRestaurant: Int
//    let bestRatedRestaurant: [BestRatedRestaurant]
//    let experts: [Expert]
//
//    enum CodingKeys: String, CodingKey {
//        case popularity
//        case nightlifeIndex
//        case nearbyRes
//        case topCuisines
//        case popularityRes
//        case nightlifeRes
//        case subzone
//        case subzoneID
//        case city, location
//        case numRestaurant
//        case bestRatedRestaurant
//        case experts
//    }
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.bestRatedRestaurantTask(with: url) { bestRatedRestaurant, response, error in
////     if let bestRatedRestaurant = bestRatedRestaurant {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - BestRatedRestaurant
//struct BestRatedRestaurant: Codable {
//    let restaurant: Restaurant
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.restaurantTask(with: url) { restaurant, response, error in
////     if let restaurant = restaurant {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - Restaurant
//struct Restaurant: Codable {
//    let r: R
//    let apikey: Apikey
//    let id, name: String
//    let url: String
//    let location: RestaurantLocation
//    let switchToOrderMenu: Int
//    let cuisines, timings: String
//    let averageCostForTwo, priceRange: Int
//    let currency: Currency
//    let highlights: [String]
//    let offers: [JSONAny]
//    let opentableSupport, isZomatoBookRes: Int
//    let mezzoProvider: MezzoProvider
//    let isBookFormWebView: Int
//    let bookFormWebViewURL: String
//    let bookAgainURL: String?
//    let thumb: String
//    let userRating: UserRating
//    let allReviewsCount: Int
//    let photosURL: String
//    let photoCount: Int
//    let menuURL: String
//    let featuredImage: String
//    let medioProvider: MedioProvider
//    let hasOnlineDelivery, isDeliveringNow: Int
//    let storeType: String
//    let includeBogoOffers: Bool
//    let deeplink: String
//    let isTableReservationSupported, hasTableBooking: Int
//    let eventsURL: String
//    let phoneNumbers: String
//    let allReviews: AllReviews
//    let establishment: [String]
//    let orderURL: String?
//    let orderDeeplink: String?
//    let bookURL: String?
//    let zomatoEvents: [ZomatoEvent]?
//
//    enum CodingKeys: String, CodingKey {
//        case r
//        case apikey, id, name, url, location
//        case switchToOrderMenu
//        case cuisines, timings
//        case averageCostForTwo
//        case priceRange
//        case currency, highlights, offers
//        case opentableSupport
//        case isZomatoBookRes
//        case mezzoProvider
//        case isBookFormWebView
//        case bookFormWebViewURL
//        case bookAgainURL
//        case thumb
//        case userRating
//        case allReviewsCount
//        case photosURL
//        case photoCount
//        case menuURL
//        case featuredImage
//        case medioProvider
//        case hasOnlineDelivery
//        case isDeliveringNow
//        case storeType
//        case includeBogoOffers
//        case deeplink
//        case isTableReservationSupported
//        case hasTableBooking
//        case eventsURL
//        case phoneNumbers
//        case allReviews
//        case establishment
//        case orderURL
//        case orderDeeplink
//        case bookURL
//        case zomatoEvents
//    }
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.allReviewsTask(with: url) { allReviews, response, error in
////     if let allReviews = allReviews {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - AllReviews
//struct AllReviews: Codable {
//    let reviews: [Review]
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.reviewTask(with: url) { review, response, error in
////     if let review = review {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - Review
//struct Review: Codable {
//    let review: [JSONAny]
//}
//
//enum Apikey: String, Codable {
//    case the0Ab207444De5B986697Ae0498318535D = "0ab207444de5b986697ae0498318535d"
//}
//
//enum Currency: String, Codable {
//    case rs = "Rs."
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.restaurantLocationTask(with: url) { restaurantLocation, response, error in
////     if let restaurantLocation = restaurantLocation {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - RestaurantLocation
//struct RestaurantLocation: Codable {
//    let address: String
//    let locality: Locality
//    let city: City
//    let cityID: Int
//    let latitude, longitude, zipcode: String
//    let countryID: Int
//    let localityVerbose: TitleEnum
//
//    enum CodingKeys: String, CodingKey {
//        case address, locality, city
//        case cityID
//        case latitude, longitude, zipcode
//        case countryID
//        case localityVerbose
//    }
//}
//
//enum City: String, Codable {
//    case bangalore = "Bangalore"
//}
//
//enum Locality: String, Codable {
//    case jwMarriottBengaluruLavelleRoad = "JW Marriott Bengaluru, Lavelle Road"
//    case lavelleRoad = "Lavelle Road"
//    case ubCity = "UB City"
//}
//
//enum TitleEnum: String, Codable {
//    case jwMarriottBengaluruLavelleRoadBangalore = "JW Marriott Bengaluru, Lavelle Road, Bangalore"
//    case lavelleRoadBangalore = "Lavelle Road, Bangalore"
//    case ubCityBangalore = "UB City, Bangalore"
//}
//
//enum MedioProvider: Codable {
//    case bool(Bool)
//    case string(String)
//
//    init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if let x = try? container.decode(Bool.self) {
//            self = .bool(x)
//            return
//        }
//        if let x = try? container.decode(String.self) {
//            self = .string(x)
//            return
//        }
//        throw DecodingError.typeMismatch(MedioProvider.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for MedioProvider"))
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        switch self {
//        case .bool(let x):
//            try container.encode(x)
//        case .string(let x):
//            try container.encode(x)
//        }
//    }
//}
//
//enum MezzoProvider: String, Codable {
//    case other = "OTHER"
//    case zomatoBook = "ZOMATO_BOOK"
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.rTask(with: url) { r, response, error in
////     if let r = r {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - R
//struct R: Codable {
//    let resID: Int
//    let isGroceryStore: Bool
//    let hasMenuStatus: HasMenuStatus
//
//    enum CodingKeys: String, CodingKey {
//        case resID
//        case isGroceryStore
//        case hasMenuStatus
//    }
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.hasMenuStatusTask(with: url) { hasMenuStatus, response, error in
////     if let hasMenuStatus = hasMenuStatus {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - HasMenuStatus
//struct HasMenuStatus: Codable {
//    let delivery: Delivery
//    let takeaway: Int
//}
//
//enum Delivery: Codable {
//    case bool(Bool)
//    case integer(Int)
//
//    init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if let x = try? container.decode(Bool.self) {
//            self = .bool(x)
//            return
//        }
//        if let x = try? container.decode(Int.self) {
//            self = .integer(x)
//            return
//        }
//        throw DecodingError.typeMismatch(Delivery.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Delivery"))
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        switch self {
//        case .bool(let x):
//            try container.encode(x)
//        case .integer(let x):
//            try container.encode(x)
//        }
//    }
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.userRatingTask(with: url) { userRating, response, error in
////     if let userRating = userRating {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - UserRating
//struct UserRating: Codable {
//    let aggregateRating: String
//    let ratingText: RatingText
//    let ratingColor: RatingColor
//    let ratingObj: RatingObj
//    let votes: Int
//
//    enum CodingKeys: String, CodingKey {
//        case aggregateRating
//        case ratingText
//        case ratingColor
//        case ratingObj
//        case votes
//    }
//}
//
//enum RatingColor: String, Codable {
//    case the3F7E00 = "3F7E00"
//    case the5Ba829 = "5BA829"
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.ratingObjTask(with: url) { ratingObj, response, error in
////     if let ratingObj = ratingObj {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - RatingObj
//struct RatingObj: Codable {
//    let title: TitleClass
//    let bgColor: BgColor
//
//    enum CodingKeys: String, CodingKey {
//        case title
//        case bgColor
//    }
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.bgColorTask(with: url) { bgColor, response, error in
////     if let bgColor = bgColor {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - BgColor
//struct BgColor: Codable {
//    let type: TypeEnum
//    let tint: String
//}
//
//enum TypeEnum: String, Codable {
//    case lime = "lime"
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.titleClassTask(with: url) { titleClass, response, error in
////     if let titleClass = titleClass {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - TitleClass
//struct TitleClass: Codable {
//    let text: String
//}
//
//enum RatingText: String, Codable {
//    case excellent = "Excellent"
//    case veryGood = "Very Good"
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.zomatoEventTask(with: url) { zomatoEvent, response, error in
////     if let zomatoEvent = zomatoEvent {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - ZomatoEvent
//struct ZomatoEvent: Codable {
//    let event: Event
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.eventTask(with: url) { event, response, error in
////     if let event = event {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - Event
//struct Event: Codable {
//    let eventID: Int
//    let friendlyStartDate, friendlyEndDate, friendlyTimingStr, startDate: String
//    let endDate, endTime, startTime: String
//    let isActive: Int
//    let dateAdded: String
//    let photos: [PhotoElement]
//    let restaurants: [JSONAny]
//    let isValid: Int
//    let shareURL: String
//    let showShareURL: Int
//    let title, eventDescription, displayTime, displayDate: String
//    let isEndTimeSet: Int
//    let disclaimer: String
//    let eventCategory: Int
//    let eventCategoryName, bookLink: String
//    let types: [TypeElement]
//    let shareData: ShareData
//
//    enum CodingKeys: String, CodingKey {
//        case eventID
//        case friendlyStartDate
//        case friendlyEndDate
//        case friendlyTimingStr
//        case startDate
//        case endDate
//        case endTime
//        case startTime
//        case isActive
//        case dateAdded
//        case photos, restaurants
//        case isValid
//        case shareURL
//        case showShareURL
//        case title
//        case eventDescription
//        case displayTime
//        case displayDate
//        case isEndTimeSet
//        case disclaimer
//        case eventCategory
//        case eventCategoryName
//        case bookLink
//        case types
//        case shareData
//    }
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.photoElementTask(with: url) { photoElement, response, error in
////     if let photoElement = photoElement {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - PhotoElement
//struct PhotoElement: Codable {
//    let photo: PhotoPhoto
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.photoPhotoTask(with: url) { photoPhoto, response, error in
////     if let photoPhoto = photoPhoto {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - PhotoPhoto
//struct PhotoPhoto: Codable {
//    let url, thumbURL: String
//    let order: Int
//    let md5Sum: String
//    let id, photoID, uuid: Int
//    let type: String
//
//    enum CodingKeys: String, CodingKey {
//        case url
//        case thumbURL
//        case order
//        case md5Sum
//        case id
//        case photoID
//        case uuid, type
//    }
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.shareDataTask(with: url) { shareData, response, error in
////     if let shareData = shareData {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - ShareData
//struct ShareData: Codable {
//    let shouldShow: Int
//
//    enum CodingKeys: String, CodingKey {
//        case shouldShow
//    }
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.typeElementTask(with: url) { typeElement, response, error in
////     if let typeElement = typeElement {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - TypeElement
//struct TypeElement: Codable {
//    let name, color: String
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.expertTask(with: url) { expert, response, error in
////     if let expert = expert {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - Expert
//struct Expert: Codable {
//    let user: User
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.userTask(with: url) { user, response, error in
////     if let user = user {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - User
//struct User: Codable {
//    let name, foodieColor: String
//    let profileURL: String
//    let profileImage: String
//    let profileDeeplink: String
//
//    enum CodingKeys: String, CodingKey {
//        case name
//        case foodieColor
//        case profileURL
//        case profileImage
//        case profileDeeplink
//    }
//}
//
////
//// To read values from URLs:
////
////   let task = URLSession.shared.welcomeLocationTask(with: url) { welcomeLocation, response, error in
////     if let welcomeLocation = welcomeLocation {
////       ...
////     }
////   }
////   task.resume()
//
//// MARK: - WelcomeLocation
//struct WelcomeLocation: Codable {
//    let entityType: String
//    let entityID: Int
//    let title: TitleEnum
//    let latitude, longitude: Double
//    let cityID: Int
//    let cityName: String
//    let countryID: Int
//    let countryName: String
//
//    enum CodingKeys: String, CodingKey {
//        case entityType
//        case entityID
//        case title, latitude, longitude
//        case cityID
//        case cityName
//        case countryID
//        case countryName
//    }
//}
//
//// MARK: - Helper functions for creating encoders and decoders
//
//func newJSONDecoder() -> JSONDecoder {
//    let decoder = JSONDecoder()
//    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
//        decoder.dateDecodingStrategy = .iso8601
//    }
//    return decoder
//}
//
//func newJSONEncoder() -> JSONEncoder {
//    let encoder = JSONEncoder()
//    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
//        encoder.dateEncodingStrategy = .iso8601
//    }
//    return encoder
//}
//
//// MARK: - URLSession response handlers
//
//extension URLSession {
//    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
//        return self.dataTask(with: url) { data, response, error in
//            guard let data = data, error == nil else {
//                completionHandler(nil, response, error)
//                return
//            }
//            completionHandler(try? newJSONDecoder().decode(T.self, from: data), response, nil)
//        }
//    }
//
//    func welcomeTask(with url: URL, completionHandler: @escaping (Welcome?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
//        return self.codableTask(with: url, completionHandler: completionHandler)
//    }
//}
//
//// MARK: - Encode/decode helpers
//
//class JSONNull: Codable, Hashable {
//
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//
//    public var hashValue: Int {
//        return 0
//    }
//
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}
//
//class JSONCodingKey: CodingKey {
//    let key: String
//
//    required init?(intValue: Int) {
//        return nil
//    }
//
//    required init?(stringValue: String) {
//        key = stringValue
//    }
//
//    var intValue: Int? {
//        return nil
//    }
//
//    var stringValue: String {
//        return key
//    }
//}
//
//class JSONAny: Codable {
//
//    let value: Any
//
//    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
//        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
//        return DecodingError.typeMismatch(JSONAny.self, context)
//    }
//
//    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
//        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
//        return EncodingError.invalidValue(value, context)
//    }
//
//    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
//        if let value = try? container.decode(Bool.self) {
//            return value
//        }
//        if let value = try? container.decode(Int64.self) {
//            return value
//        }
//        if let value = try? container.decode(Double.self) {
//            return value
//        }
//        if let value = try? container.decode(String.self) {
//            return value
//        }
//        if container.decodeNil() {
//            return JSONNull()
//        }
//        throw decodingError(forCodingPath: container.codingPath)
//    }
//
//    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
//        if let value = try? container.decode(Bool.self) {
//            return value
//        }
//        if let value = try? container.decode(Int64.self) {
//            return value
//        }
//        if let value = try? container.decode(Double.self) {
//            return value
//        }
//        if let value = try? container.decode(String.self) {
//            return value
//        }
//        if let value = try? container.decodeNil() {
//            if value {
//                return JSONNull()
//            }
//        }
//        if var container = try? container.nestedUnkeyedContainer() {
//            return try decodeArray(from: &container)
//        }
//        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
//            return try decodeDictionary(from: &container)
//        }
//        throw decodingError(forCodingPath: container.codingPath)
//    }
//
//    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
//        if let value = try? container.decode(Bool.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decode(Int64.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decode(Double.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decode(String.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decodeNil(forKey: key) {
//            if value {
//                return JSONNull()
//            }
//        }
//        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
//            return try decodeArray(from: &container)
//        }
//        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
//            return try decodeDictionary(from: &container)
//        }
//        throw decodingError(forCodingPath: container.codingPath)
//    }
//
//    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
//        var arr: [Any] = []
//        while !container.isAtEnd {
//            let value = try decode(from: &container)
//            arr.append(value)
//        }
//        return arr
//    }
//
//    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
//        var dict = [String: Any]()
//        for key in container.allKeys {
//            let value = try decode(from: &container, forKey: key)
//            dict[key.stringValue] = value
//        }
//        return dict
//    }
//
//    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
//        for value in array {
//            if let value = value as? Bool {
//                try container.encode(value)
//            } else if let value = value as? Int64 {
//                try container.encode(value)
//            } else if let value = value as? Double {
//                try container.encode(value)
//            } else if let value = value as? String {
//                try container.encode(value)
//            } else if value is JSONNull {
//                try container.encodeNil()
//            } else if let value = value as? [Any] {
//                var container = container.nestedUnkeyedContainer()
//                try encode(to: &container, array: value)
//            } else if let value = value as? [String: Any] {
//                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
//                try encode(to: &container, dictionary: value)
//            } else {
//                throw encodingError(forValue: value, codingPath: container.codingPath)
//            }
//        }
//    }
//
//    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
//        for (key, value) in dictionary {
//            let key = JSONCodingKey(stringValue: key)!
//            if let value = value as? Bool {
//                try container.encode(value, forKey: key)
//            } else if let value = value as? Int64 {
//                try container.encode(value, forKey: key)
//            } else if let value = value as? Double {
//                try container.encode(value, forKey: key)
//            } else if let value = value as? String {
//                try container.encode(value, forKey: key)
//            } else if value is JSONNull {
//                try container.encodeNil(forKey: key)
//            } else if let value = value as? [Any] {
//                var container = container.nestedUnkeyedContainer(forKey: key)
//                try encode(to: &container, array: value)
//            } else if let value = value as? [String: Any] {
//                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
//                try encode(to: &container, dictionary: value)
//            } else {
//                throw encodingError(forValue: value, codingPath: container.codingPath)
//            }
//        }
//    }
//
//    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
//        if let value = value as? Bool {
//            try container.encode(value)
//        } else if let value = value as? Int64 {
//            try container.encode(value)
//        } else if let value = value as? Double {
//            try container.encode(value)
//        } else if let value = value as? String {
//            try container.encode(value)
//        } else if value is JSONNull {
//            try container.encodeNil()
//        } else {
//            throw encodingError(forValue: value, codingPath: container.codingPath)
//        }
//    }
//
//    public required init(from decoder: Decoder) throws {
//        if var arrayContainer = try? decoder.unkeyedContainer() {
//            self.value = try JSONAny.decodeArray(from: &arrayContainer)
//        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
//            self.value = try JSONAny.decodeDictionary(from: &container)
//        } else {
//            let container = try decoder.singleValueContainer()
//            self.value = try JSONAny.decode(from: container)
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        if let arr = self.value as? [Any] {
//            var container = encoder.unkeyedContainer()
//            try JSONAny.encode(to: &container, array: arr)
//        } else if let dict = self.value as? [String: Any] {
//            var container = encoder.container(keyedBy: JSONCodingKey.self)
//            try JSONAny.encode(to: &container, dictionary: dict)
//        } else {
//            var container = encoder.singleValueContainer()
//            try JSONAny.encode(to: &container, value: self.value)
//        }
//    }
//}
//
