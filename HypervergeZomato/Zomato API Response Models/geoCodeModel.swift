// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

//
// To read values from URLs:
//
//   let task = URLSession.shared.welcomeTask(with: url) { welcome, response, error in
//     if let welcome = welcome {
//       ...
//     }
//   }
//   task.resume()

import Foundation

// MARK: - Welcome
struct Welcome: Codable {
    let location: WelcomeLocation
    let popularity: Popularity
    let link: String
    let nearbyRestaurants: [NearbyRestaurant]

    enum CodingKeys: String, CodingKey {
        case location, popularity, link
        case nearbyRestaurants = "nearby_restaurants"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.welcomeLocationTask(with: url) { welcomeLocation, response, error in
//     if let welcomeLocation = welcomeLocation {
//       ...
//     }
//   }
//   task.resume()

// MARK: - WelcomeLocation
struct WelcomeLocation: Codable {
    let entityType: String
    let entityID: Int
    let title, latitude, longitude: String
    let cityID: Int
    let cityName: String
    let countryID: Int
    let countryName: String

    enum CodingKeys: String, CodingKey {
        case entityType = "entity_type"
        case entityID = "entity_id"
        case title, latitude, longitude
        case cityID = "city_id"
        case cityName = "city_name"
        case countryID = "country_id"
        case countryName = "country_name"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.nearbyRestaurantTask(with: url) { nearbyRestaurant, response, error in
//     if let nearbyRestaurant = nearbyRestaurant {
//       ...
//     }
//   }
//   task.resume()

// MARK: - NearbyRestaurant
struct NearbyRestaurant: Codable {
    let restaurant: Restaurant
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.restaurantTask(with: url) { restaurant, response, error in
//     if let restaurant = restaurant {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Restaurant
struct Restaurant: Codable {
    let r: R
    let apikey, id, name: String
    let url: String
    let location: RestaurantLocation
    let switchToOrderMenu: Int
    let cuisines: String
    let averageCostForTwo, priceRange: Int
    let currency: String
    let offers: [JSONAny]
    let opentableSupport, isZomatoBookRes: Int
    let mezzoProvider: String
    let isBookFormWebView: Int
    let bookFormWebViewURL, bookAgainURL: String
    let thumb: String
    let userRating: UserRating
    let photosURL, menuURL: String
    let featuredImage: String
    let medioProvider: Bool
    let hasOnlineDelivery, isDeliveringNow: Int
    let storeType: String
    let includeBogoOffers: Bool
    let deeplink: String
    let isTableReservationSupported, hasTableBooking: Int
    let eventsURL: String
    let orderURL: String?
    let orderDeeplink: String?

    enum CodingKeys: String, CodingKey {
        case r = "R"
        case apikey, id, name, url, location
        case switchToOrderMenu = "switch_to_order_menu"
        case cuisines
        case averageCostForTwo = "average_cost_for_two"
        case priceRange = "price_range"
        case currency, offers
        case opentableSupport = "opentable_support"
        case isZomatoBookRes = "is_zomato_book_res"
        case mezzoProvider = "mezzo_provider"
        case isBookFormWebView = "is_book_form_web_view"
        case bookFormWebViewURL = "book_form_web_view_url"
        case bookAgainURL = "book_again_url"
        case thumb
        case userRating = "user_rating"
        case photosURL = "photos_url"
        case menuURL = "menu_url"
        case featuredImage = "featured_image"
        case medioProvider = "medio_provider"
        case hasOnlineDelivery = "has_online_delivery"
        case isDeliveringNow = "is_delivering_now"
        case storeType = "store_type"
        case includeBogoOffers = "include_bogo_offers"
        case deeplink
        case isTableReservationSupported = "is_table_reservation_supported"
        case hasTableBooking = "has_table_booking"
        case eventsURL = "events_url"
        case orderURL = "order_url"
        case orderDeeplink = "order_deeplink"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.restaurantLocationTask(with: url) { restaurantLocation, response, error in
//     if let restaurantLocation = restaurantLocation {
//       ...
//     }
//   }
//   task.resume()

// MARK: - RestaurantLocation
struct RestaurantLocation: Codable {
    let address, locality, city: String
    let cityID: Int
    let latitude, longitude, zipcode: String
    let countryID: Int
    let localityVerbose: String

    enum CodingKeys: String, CodingKey {
        case address, locality, city
        case cityID = "city_id"
        case latitude, longitude, zipcode
        case countryID = "country_id"
        case localityVerbose = "locality_verbose"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.rTask(with: url) { r, response, error in
//     if let r = r {
//       ...
//     }
//   }
//   task.resume()

// MARK: - R
struct R: Codable {
    let resID: Int
    let hasMenuStatus: HasMenuStatus
    let isGroceryStore: Bool?

    enum CodingKeys: String, CodingKey {
        case resID = "res_id"
        case hasMenuStatus = "has_menu_status"
        case isGroceryStore = "is_grocery_store"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.hasMenuStatusTask(with: url) { hasMenuStatus, response, error in
//     if let hasMenuStatus = hasMenuStatus {
//       ...
//     }
//   }
//   task.resume()

// MARK: - HasMenuStatus
struct HasMenuStatus: Codable {
    let delivery: Delivery
    let takeaway: Int
}

enum Delivery: Codable {
    case bool(Bool)
    case integer(Int)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Bool.self) {
            self = .bool(x)
            return
        }
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        throw DecodingError.typeMismatch(Delivery.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Delivery"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .bool(let x):
            try container.encode(x)
        case .integer(let x):
            try container.encode(x)
        }
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.userRatingTask(with: url) { userRating, response, error in
//     if let userRating = userRating {
//       ...
//     }
//   }
//   task.resume()

// MARK: - UserRating
struct UserRating: Codable {
    let aggregateRating: AggregateRating
    let ratingText, ratingColor: String
    let ratingObj: RatingObj
    let votes: Int

    enum CodingKeys: String, CodingKey {
        case aggregateRating = "aggregate_rating"
        case ratingText = "rating_text"
        case ratingColor = "rating_color"
        case ratingObj = "rating_obj"
        case votes
    }
}

enum AggregateRating: Codable {
    case integer(Int)
    case string(String)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(AggregateRating.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for AggregateRating"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.ratingObjTask(with: url) { ratingObj, response, error in
//     if let ratingObj = ratingObj {
//       ...
//     }
//   }
//   task.resume()

// MARK: - RatingObj
struct RatingObj: Codable {
    let title: Title
    let bgColor: BgColor

    enum CodingKeys: String, CodingKey {
        case title
        case bgColor = "bg_color"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.bgColorTask(with: url) { bgColor, response, error in
//     if let bgColor = bgColor {
//       ...
//     }
//   }
//   task.resume()

// MARK: - BgColor
struct BgColor: Codable {
    let type, tint: String
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.titleTask(with: url) { title, response, error in
//     if let title = title {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Title
struct Title: Codable {
    let text: String
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.popularityTask(with: url) { popularity, response, error in
//     if let popularity = popularity {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Popularity
struct Popularity: Codable {
    let popularity, nightlifeIndex: String
    let nearbyRes, topCuisines: [String]
    let popularityRes, nightlifeRes, subzone: String
    let subzoneID: Int
    let city: String

    enum CodingKeys: String, CodingKey {
        case popularity
        case nightlifeIndex = "nightlife_index"
        case nearbyRes = "nearby_res"
        case topCuisines = "top_cuisines"
        case popularityRes = "popularity_res"
        case nightlifeRes = "nightlife_res"
        case subzone
        case subzoneID = "subzone_id"
        case city
    }
}

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - URLSession response handlers

extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            completionHandler(try? newJSONDecoder().decode(T.self, from: data), response, nil)
        }
    }

    func welcomeTask(with url: URL, completionHandler: @escaping (Welcome?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
