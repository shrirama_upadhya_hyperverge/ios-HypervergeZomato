//
//  CameraViewController.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 18/02/21.
//

import UIKit
import FirebaseFirestore
class SignInCameraViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    var db = Firestore.firestore()
    var imagePicker: UIImagePickerController!
    
    @IBOutlet var ivSelfie: UIImageView!
    @IBOutlet var capturePhoto: UIButton!
    @IBOutlet var proceedButton: UIButton!
    @IBOutlet var progress: UIActivityIndicatorView!
    
    var selfieImage: UIImage?
    var isMatch: Bool?
    var imageFromFirebase: UIImage?
    
    var panNo: String?
    var panName : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        title="SignIn - Capture Selfie"
        
        capturePhoto.clipsToBounds = true
        capturePhoto.layer.cornerRadius = 10
        
        proceedButton.clipsToBounds = true
        proceedButton.layer.cornerRadius = 10
        
        ivSelfie.clipsToBounds = true
        ivSelfie.layer.cornerRadius = 8
        
        self.progress.isHidden = true
        self.proceedButton.isHidden = true
        
        
        self.view.showToast(toastMessage: "Capture Selfie by clicking on capture button", duration: 2.0)
        
        let defaults = UserDefaults.standard
        if let stringOne = defaults.string(forKey: "panNo") {
            print(stringOne) // Some String Value
            self.panNo = stringOne
        }
        if let stringTwo = defaults.string(forKey: "panName") {
            print(stringTwo) // Another String Value
            self.panName = stringTwo
        }
        
    }
    
    @IBAction func onCaptureTap(){
        self.progress.isHidden = false
        self.progress.startAnimating()
        self.capturePhoto.isHidden = true
        
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
        self.progress.isHidden = true
        self.progress.stopAnimating()
        self.progress.isHidden = false
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        let img = info[.originalImage] as? UIImage
        self.selfieImage = info[.originalImage] as? UIImage
        ivSelfie.image = UIImage(cgImage: (img?.cgImage)!, scale: 1.0, orientation: .downMirrored)
        ivSelfie.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
        //        ivSelfie.image = info[.originalImage] as? UIImage
        
        title = "SignIn - Review Selfie"
        self.progress.isHidden = true
        self.progress.stopAnimating()
        self.proceedButton.isHidden = false
    }
    
    
    @IBAction func onProceedTap(_ sender: Any) {
        DispatchQueue.main.async {
            self.progress.isHidden = false
            self.progress.startAnimating()
            self.proceedButton.isHidden = true
        }
        
        let docRef = db.collection("RegisteredUsers").document("\(self.panNo! as String)")
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let imageURLString = document.get("Image") as! String
                
                let myURL = URL(string: imageURLString)
                if let data = try? Data(contentsOf: myURL!)
                    {
                        self.imageFromFirebase = UIImage(data: data)!
                    }
                
                let boundary = "Boundary-\(UUID().uuidString)"
                var request = URLRequest(url: URL(string: "https://ind-faceid.hyperverge.co/v1/photo/verifyPair")!,timeoutInterval: Double.infinity)
                request.addValue("857acb", forHTTPHeaderField: "appId")
                request.addValue("e0d888e703691bccbf92", forHTTPHeaderField: "appKey")
                request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
                request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                
                let panImageData = self.imageFromFirebase!.jpegData(compressionQuality: 0.7)! as NSData
                let selfieImageData = self.selfieImage!.jpegData(compressionQuality: 0.7)! as NSData
                
                let imageDataDict = ["selfie" : panImageData, "selfie2" : selfieImageData]
                
                request.httpMethod = "POST"
                request.httpBody = self.createBodyWithParameters(filePathKey: "file", imageDataKey: imageDataDict, boundary: boundary) as Data
                
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let dataResponse = data else {
                        print(String(describing: error))
                        return
                    }
                    do{
                        //here dataResponse received from a network request
                        let jsonResponse = try JSONSerialization.jsonObject(with:
                                                                                dataResponse, options: [])
                        guard let jsonObject = jsonResponse as? [String: Any] else {
                            return
                        }
                        guard let result = jsonObject["result"] as? [String:Any] else { return }
                        print(result)
                        if(result["match"] as! String == "yes"){
                            self.isMatch=true
                            DispatchQueue.main.async {
                                self.title = "SignIn - Capture Selfie"
                                self.progress.isHidden = true
                                self.progress.stopAnimating()
                                self.proceedButton.isHidden = true
                                self.capturePhoto.isHidden = false
                                self.ivSelfie.image = UIImage(named: "dummy selfie.jpg")
                                self.ivSelfie.transform = .identity
                                
                                let defaults = UserDefaults.standard
                                defaults.set("yes", forKey: "loggedIn")
                                
                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabView") as? TabViewController
                                //                        vc?.userPanNo = self.panNo
                                //                        vc?.userName = self.userName
                                self.navigationController?.pushViewController(vc!, animated: true)
                            }
                            
                        }else{
                            self.isMatch=false
                            DispatchQueue.main.async {
                                self.view.showToast(toastMessage: "Database Image and Selfie do not match, Please try again", duration: 2.0)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                                    // your code here
                                    self.title = "SignIn - Capture Selfie"
                                    self.progress.isHidden = true
                                    self.progress.stopAnimating()
                                    self.proceedButton.isHidden = true
                                    self.capturePhoto.isHidden = false
                                    self.ivSelfie.image = UIImage(named: "dummy selfie.jpg")
                                    self.ivSelfie.transform = .identity
                                }
                                
                            }
                            
                            
                            
                        }
                    } catch let parsingError {
                        print("Error", parsingError)
                    }
                }
                
                task.resume()

                

            } else {
                print("Document does not exist")
            }
        }
        
        
        
        
    }
    
    func createBodyWithParameters(filePathKey: String?, imageDataKey: [String : NSData], boundary: String) -> NSData{
        let body = NSMutableData();
        
        for(k, value) in imageDataKey{
            let key = k
            let filename = "\(key).jpg"
            let mimetype = "image/jpg"
            
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
            body.appendString("Content-Type: \(mimetype)\r\n\r\n")
            body.append(value as Data)
            body.appendString("\r\n")
            
        }
        body.appendString("--\(boundary)--\r\n")
        
        return body
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
