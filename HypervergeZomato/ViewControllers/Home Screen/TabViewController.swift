//
//  TabViewController.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 18/02/21.
//

import UIKit

class TabViewController: UITabBarController {
    
    var userPanNo: String?
    var userName: String?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        title="HypervergeZomato"
        
//        self.navigationController?.viewControllers = [self]
        self.tabBar.unselectedItemTintColor = UIColor(red: 73.00/255.0, green: 213.00/255.0, blue: 208.00/255.0, alpha: 1.00)

        guard let homeTab = self.viewControllers?[0] as? HomeViewController else{return}
        guard let dashboardTab = self.viewControllers?[1] as? DashboardViewController else{return}
        guard let orderHistoryTab = self.viewControllers?[2] as? OrderHistoryViewController else{return}
        
        let defaults = UserDefaults.standard
        if let stringOne = defaults.string(forKey: "panNo") {
            print(stringOne) // Some String Value
            self.userPanNo = stringOne
        }
        if let stringTwo = defaults.string(forKey: "panName") {
            print(stringTwo) // Another String Value
            self.userName = stringTwo
        }
        
        homeTab.userPanNo = self.userPanNo
        homeTab.userName = self.userName
        
        dashboardTab.userPanNo = self.userPanNo
        dashboardTab.userName = self.userName
        
        orderHistoryTab.userPanNo = self.userPanNo
        orderHistoryTab.userName = self.userName
        
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
