//
//  HomeViewController.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 18/02/21.
//

import UIKit
import MapKit
import CoreLocation
import SQLite

class HomeViewController: UIViewController, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource{
    
    lazy var locationManager: CLLocationManager = {
            var manager = CLLocationManager()
            manager.distanceFilter = 10
            manager.desiredAccuracy = kCLLocationAccuracyBest
            return manager
        }()
    
    var userPanNo: String?
    var userName: String?
    var locLatitude: Double?
    var locLongitude: Double?
    var addLocality: String?
    @IBOutlet var tableView: UITableView!
    @IBOutlet var progress: UIActivityIndicatorView!
    
    @IBOutlet weak var mapCloseButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    var restCards: [RestCard] = []
    var ratings: [Float] = []
    
    var database: Connection!
    let restaurantsTable = Table("restaurants")
    let id = Expression<Int>("id")
    let name = Expression<String>("restName")
    let cuisine = Expression<String>("restCuisine")
    let rating = Expression<String>("restRating")
    let image = Expression<String>("restString")
     
    
    var isSorted: Bool!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.leftBarButtonItem?.action = #selector(showMap)
        self.navigationItem.rightBarButtonItem?.action = #selector(filterRestaurants)
        self.tableView.isHidden = false
        self.mapView.isHidden = true
        self.mapCloseButton.isHidden = true
        self.tableView.reloadData()
        let cells = self.tableView.visibleCells
        let tableHeight: CGFloat = self.tableView.bounds.size.height
        
        for i in cells {
            let cell: RestaurantCardCell = i as! RestaurantCardCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: RestaurantCardCell = a as! RestaurantCardCell
            UIView.animate(withDuration: 2.0, delay: 0.1 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            
            index += 1
        }
        
        
        guard let navigationController = self.navigationController else { return }
        var navigationArray = navigationController.viewControllers // To get all UIViewController stack as Array
        let temp = navigationArray.last
        navigationArray.removeAll()
        navigationArray.append(temp!) //To remove all previous UIViewController except the last one
        self.navigationController?.viewControllers = navigationArray
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        title = "Home"
//        self.navigationController?.viewControllers = [self]
        self.view.showToast(toastMessage: "Showing nearby restuarants", duration: 1.0)

        let defaults = UserDefaults.standard
        if let stringOne = defaults.string(forKey: "panNo") {
            self.userPanNo = stringOne
        }
        if let stringTwo = defaults.string(forKey: "panName") {
            self.userName = stringTwo
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "filter.svg"), style: .plain,  target: self, action: #selector(filterRestaurants))
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "location-pin.svg"), style: .plain,  target: self, action: #selector(showMap))
//        self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 255.00, green: 255.00, blue: 255.00, alpha: 1.00)
//        self.navigationController?.navigationBar.barTintColor = UIColor(red: 237.00, green: 88.00, blue: 133.00, alpha: 1.00)
//        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor(red: 255.00, green: 255.00, blue: 255.00, alpha: 1.00)]

        self.progress.isHidden = false
        self.progress.startAnimating()
        self.tableView.isHidden = true
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        
        do{
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileURL = documentDirectory.appendingPathComponent("restaurants").appendingPathExtension("sqlite3")
            let db = try Connection(fileURL.path)
            self.database = db
            
            let createTable = self.restaurantsTable.create { (table) in
                table.column(self.id, primaryKey: .autoincrement)
                table.column(self.name)
                table.column(self.cuisine)
                table.column(self.image)
                table.column(self.rating)
            }
            do{
                try self.database.run(self.restaurantsTable.drop(ifExists: true))
                try self.database.run(createTable)
                
            }catch{
                print(error)
            }
        }catch{
            print(error)
        }
        
        
    }
    @objc func filterRestaurants(){
        if(self.isSorted){
            self.restCards = self.restCards.sorted(by: { $0.rating < $1.rating })
            self.isSorted = false
            
//            self.tableView.reloadData()
//            let cells = self.tableView.visibleCells
//            let tableHeight: CGFloat = self.tableView.bounds.size.height
//
//            for i in cells {
//                let cell: RestaurantCardCell = i as! RestaurantCardCell
//                cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
//            }
//
//            var index = 0
//
//            for a in cells {
//                let cell: RestaurantCardCell = a as! RestaurantCardCell
//                UIView.animate(withDuration: 2.0, delay: 0.1 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
//                    cell.transform = CGAffineTransform(translationX: 0, y: 0);
//                }, completion: nil)
//
//                index += 1
//            }
            self.view.showToast(toastMessage: "Showing Ascending order (Filter : Rating)", duration: 1.0)
            
        }
        else{
            self.restCards = self.restCards.sorted(by: { $0.rating > $1.rating })
            self.isSorted = true
//            self.tableView.reloadData()
//            let cells = self.tableView.visibleCells
//            let tableHeight: CGFloat = self.tableView.bounds.size.height
//
//            for i in cells {
//                let cell: RestaurantCardCell = i as! RestaurantCardCell
//                cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
//            }
//
//            var index = 0
//
//            for a in cells {
//                let cell: RestaurantCardCell = a as! RestaurantCardCell
//                UIView.animate(withDuration: 2.0, delay: 0.1 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
//                    cell.transform = CGAffineTransform(translationX: 0, y: 0);
//                }, completion: nil)
//
//                index += 1
//            }
            self.view.showToast(toastMessage: "Showing Descending order (Filter : Rating)", duration: 1.0)
        }
        self.tableView.reloadData()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        manager.stopUpdatingLocation()
        self.locLatitude = locValue.latitude
        self.locLongitude = locValue.longitude
        self.locationManager.delegate = nil
        updateLocationOnMap(to: locationManager.location!, with: nil)
        getAddressFromLatLon(pdblLatitude: self.locLatitude!, withLongitude: self.locLongitude!)
        
        let queryItems = [URLQueryItem(name: "lat", value: "\(locValue.latitude)"), URLQueryItem(name: "lon", value: "\(locValue.longitude)")]
        var urlComps = URLComponents(string: "https://developers.zomato.com/api/v2.1/geocode")!
        urlComps.queryItems = queryItems
        var request = URLRequest(url: urlComps.url!)
        request.httpMethod = "GET"
        request.addValue("0ab207444de5b986697ae0498318535d", forHTTPHeaderField: "user-key")
        
        let task = URLSession.shared.dataTask(with: request) {data, response, error in
            guard let dataResponse = data else {
                print(String(describing: error))
                return
            }
            //here dataResponse received from a network request
            let welcome = try? newJSONDecoder().decode(Welcome.self, from: dataResponse)
            //            let welcomeLocation = welcome?.location
            guard let nearbyRestaurants = welcome?.nearbyRestaurants else{
                print("Error")
                return
            }
            self.restCards = []
            for nearbyRest in nearbyRestaurants{
                let restaurantDet = nearbyRest.restaurant
                var featImg: String = restaurantDet.featuredImage
                let myFloatString = (restaurantDet.userRating.ratingObj.title.text)
                let myFloat = (myFloatString as NSString).floatValue
                if(featImg == ""){
                    featImg = "https://logodownload.org/wp-content/uploads/2020/02/zomato-logo-3.png"
                }
                self.restCards.append(RestCard(name: restaurantDet.name, cuisine: restaurantDet.cuisines, image: featImg, rating: myFloat))
                
            }
            self.restCards = self.restCards.sorted(by: { $0.rating > $1.rating })
            self.isSorted = true
            
            //Inserting restaurants to table
            for restCard in self.restCards {
                let ratingString = NSString(format: "%.2f", restCard.rating) as String
                let insertRest = self.restaurantsTable.insert(self.name <- restCard.name, self.cuisine <- restCard.cuisine, self.image <- restCard.image, self.rating <- ratingString)
                do {
                    try self.database.run(insertRest)
                } catch  {
                    print(error)
                }
            }
            
            DispatchQueue.main.async{
                self.tableView.isHidden = false
                self.progress.isHidden = true
                self.progress.stopAnimating()
                self.tableView.reloadData()
                let cells = self.tableView.visibleCells
                let tableHeight: CGFloat = self.tableView.bounds.size.height
                
                for i in cells {
                    let cell: RestaurantCardCell = i as! RestaurantCardCell
                    cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
                }
                
                var index = 0
                
                for a in cells {
                    let cell: RestaurantCardCell = a as! RestaurantCardCell
                    UIView.animate(withDuration: 2.0, delay: 0.1 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
                        cell.transform = CGAffineTransform(translationX: 0, y: 0);
                    }, completion: nil)
                    
                    index += 1
                }
            }
            
        }
        
        task.resume()
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.restCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "restaurantCardCell", for: indexPath) as! RestaurantCardCell
        cell.configure(picture: self.restCards[indexPath.row].image, restaurantName: self.restCards[indexPath.row].name, restaurantCuisine: self.restCards[indexPath.row].cuisine, restaurantRating: self.restCards[indexPath.row].rating)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath)
        
        let bounds = cell!.bounds
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: .curveEaseIn, animations: {
            cell!.bounds = CGRect(x: bounds.origin.x - 20, y: bounds.origin.y, width: bounds.size.width + 60, height: bounds.size.height)
//            cell.enabled = false
        }, completion: { _ in
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RestaurantMenuView") as? RestaurantMenuViewController
            vc?.restaurantName = self.restCards[indexPath.row].name
            vc?.restaurantCuisine = self.restCards[indexPath.row].cuisine
            vc?.restaurantRating = self.restCards[indexPath.row].rating
            vc?.restaurantImage = self.restCards[indexPath.row].image
            vc?.restaurantLocation = self.addLocality
            self.navigationController?.pushViewController(vc!, animated: true)
        })
        
    }
    
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double){
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()

        let lat: Double = pdblLatitude
        //21.228124
        let lon: Double = pdblLongitude
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
                                    {(placemarks, error) in
                                        if (error != nil)
                                        {
                                            print("reverse geodcode fail: \(error!.localizedDescription)")
                                        }
                                        let pm = placemarks! as [CLPlacemark]
                                        
                                        if pm.count > 0 {
                                            let pm = placemarks![0]
                                            
                                            var addressString : String = ""
                                            if pm.subLocality != nil {
                                                addressString = addressString + pm.subLocality! + ", "
                                            }
                                            if pm.thoroughfare != nil {
                                                addressString = addressString + pm.thoroughfare! + ", "
                                            }
                                            if pm.locality != nil {
                                                addressString = addressString + pm.locality! + ", "
                                                self.addLocality = pm.locality
                                            }
                                            if pm.country != nil {
                                                addressString = addressString + pm.country! + ", "
                                            }
                                            if pm.postalCode != nil {
                                                addressString = addressString + pm.postalCode! + " "
                                            }
                                            
                                        }
                                    }
        )
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @objc func showMap(){
        self.navigationItem.leftBarButtonItem?.action = .none
        self.navigationItem.rightBarButtonItem?.action = .none
        self.tableView.isHidden = true
        self.mapView.isHidden = false
        self.mapCloseButton.isHidden = false
        self.mapCloseButton.transform = CGAffineTransform(translationX: 0, y: self.view.bounds.size.height)
        self.mapView.transform = CGAffineTransform(translationX: 0, y: self.view.bounds.size.height)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            
            self.mapView.transform = CGAffineTransform(translationX: 0, y: 0);
            self.mapCloseButton.transform = CGAffineTransform(translationX: 0, y: 0);
        }, completion: nil)
    }
    
    func updateLocationOnMap(to location: CLLocation, with title: String?) {
            let point = MKPointAnnotation()
            point.title = title
            point.coordinate = location.coordinate
            self.mapView.removeAnnotations(self.mapView.annotations)
            self.mapView.addAnnotation(point)

            let viewRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 200, longitudinalMeters: 200)
            self.mapView.setRegion(viewRegion, animated: true)
        }
    
    @IBAction func onCloseMapTap(_ sender: Any) {
        self.mapCloseButton.transform = CGAffineTransform(translationX: 0, y: 0)
        self.mapView.transform = CGAffineTransform(translationX: 0, y: 0)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            
            self.mapView.transform = CGAffineTransform(translationX: 0, y: self.view.bounds.size.height);
            self.mapCloseButton.transform = CGAffineTransform(translationX: 0, y: self.view.bounds.size.height);
        }, completion: {_ in
            self.navigationItem.leftBarButtonItem?.action = #selector(self.showMap)
            self.navigationItem.rightBarButtonItem?.action = #selector(self.filterRestaurants)
            self.tableView.isHidden = false
            self.mapView.isHidden = true
            self.mapCloseButton.isHidden = true
        })
    }
}
