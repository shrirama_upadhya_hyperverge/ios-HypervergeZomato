//
//  CheckoutViewController.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 21/02/21.
//

import UIKit
import SQLite
class CheckoutViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var clickedMenuItems: [MenuItem]!
    
    @IBOutlet weak var confirmCheckoutButton: UIButton!
    @IBOutlet weak var orderSummaryCardView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelTotalPrice: UILabel!
    
    var itemDatabase: Connection!
    let itemsTable = Table("items")
    let itemID = Expression<Int>("itemID")
    let itemName = Expression<String>("itemName")
    let itemPrice = Expression<String>("itemPrice")
    let itemImage = Expression<String>("itemImage")
    let itemQuantity = Expression<Int>("itemQuantity")
    let itemOrderID = Expression<Int>("itemOrderID")
    
    var orderIDFromMenu: Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        title = "Order Summary"
        confirmCheckoutButton.clipsToBounds = true
        confirmCheckoutButton.layer.cornerRadius = 10
        
        orderSummaryCardView.clipsToBounds = true
        orderSummaryCardView.layer.shadowColor = UIColor.gray.cgColor
        orderSummaryCardView.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        orderSummaryCardView.layer.shadowOpacity = 1.0
        orderSummaryCardView.layer.masksToBounds = false
        orderSummaryCardView.layer.cornerRadius = 20
        
        var totalPrice: Float = 0.00
        for ordIt in clickedMenuItems{
            totalPrice = totalPrice + (ordIt.price * Float(ordIt.quantity))
        }
        let s = NSString(format: "%.2f", totalPrice)
        labelTotalPrice.text = "Total Price : \u{20B9} \(s as String)"
    }
    
    @IBAction func onConfirmCheckout(_ sender: Any) {
        
        do{
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileURL = documentDirectory.appendingPathComponent("items").appendingPathExtension("sqlite3")
            let db = try Connection(fileURL.path)
            self.itemDatabase = db
            
            let createItemsTable = self.itemsTable.create { (table) in
                table.column(self.itemID, primaryKey: .autoincrement)
                table.column(self.itemName)
                table.column(self.itemImage)
                table.column(self.itemQuantity)
                table.column(self.itemPrice)
                table.column(self.itemOrderID)
            }
            do{
//                try self.itemDatabase.run(self.itemsTable.drop(ifExists: true))
                try self.itemDatabase.run(createItemsTable)
                print("created items table")
                
            }catch{
                print(error)
            }
            for it in clickedMenuItems{
                let totalPrice = it.price * Float(it.quantity)
                let str = NSString(format: "%.2f", totalPrice)
                let insertItem = self.itemsTable.insert(self.itemName <- it.name, self.itemImage <- it.image, self.itemQuantity <- it.quantity, self.itemPrice <- (str as String) ,self.itemOrderID <- self.orderIDFromMenu)
                do {
                    try self.itemDatabase.run(insertItem)
                    print("inserted item")
                } catch  {
                    print(error)
                }
            }
            
            do{
//                let it = self.itemsTable.filter(self.itemOrderID == 45)
                let its = try self.itemDatabase.prepare(self.itemsTable)
                for it in its {
                    print("itemID: \(it[self.itemID]) itemName: \(it[self.itemName]) itemImage: \(it[self.itemImage]) itemQuant: \(it[self.itemQuantity]) itemPrice: \(it[self.itemPrice]) itemOrderID: \(it[self.itemOrderID])")
                }
//                print("itemID: \(it[self.itemID]) itemName: \(it[self.itemName]) itemImage: \(it[self.itemImage]) itemQuant: \(it[self.itemQuantity]) itemPrice: \(it[self.itemPrice]) itemOrderID: \(it[self.itemOrderID])")
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PaymentSuccessfulView") as? PaymentSuccessfulViewController

            self.navigationController?.pushViewController(vc!, animated: true)
            }catch{
                print(error)
            }
            
        }catch{
            print(error)
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clickedMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderSummaryItemCell", for: indexPath) as! OrderSummaryItemCell
        cell.configure(orderItem: self.clickedMenuItems[indexPath.row])
        return cell
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
