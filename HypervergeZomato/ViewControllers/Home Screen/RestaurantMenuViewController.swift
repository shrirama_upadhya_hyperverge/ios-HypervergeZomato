//
//  RestaurantMenuViewController.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 20/02/21.
//

import UIKit
import SQLite
class RestaurantMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, TVCellDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var labelRestRating: UILabel!
    @IBOutlet weak var labelRestName: UILabel!
    @IBOutlet weak var cartCheckoutButton: UIButton!
    
    var restaurantName, restaurantCuisine: String!
    var restaurantImage: String!
    var restaurantRating: Float!
    var restaurantLocation: String!
    var orderIDForCheckout: Int!
    
    var menuItems:[MenuItem] = [
        MenuItem(name: "Dosa", price: 80.0, image: "dosa.jpeg", quantity: 0),
        MenuItem(name: "Paneer Tikka", price: 220.0, image: "paneer-tikka.jpg", quantity: 0),
        MenuItem(name: "Shahi Paneer", price: 210.0, image: "shahi paneer.jpg", quantity: 0),
        MenuItem(name: "Dal Makhani", price: 160.0, image: "dal makhani.jpg", quantity: 0),
        MenuItem(name: "Russian Salad", price: 120.0, image: "russian salad.jpg", quantity: 0),
        MenuItem(name: "Garlic Bread", price: 80.0, image: "garlic bread.jpg", quantity: 0),
        MenuItem(name: "Paneer Salsa Wrap", price: 130.0, image: "salsa wrap.jpg", quantity: 0),
        MenuItem(name: "Veg Pulao", price: 70.0, image: "veg pulao.jpg", quantity: 0)
    ]
    
    var clickedMenuItems:[MenuItem] = []
    var orderDateString: String!
    
    let maxHeaderHeight: CGFloat = 120
    let minHeaderHeight: CGFloat = 60
    var previousScrollOffset: CGFloat = 0
    
    var orderDatabase: Connection!
    let ordersTable = Table("orders")
    let orderID = Expression<Int>("orderID")
    let orderRestName = Expression<String>("orderRestName")
    let orderRestLocation = Expression<String>("orderRestLocation")
    let orderRestImage = Expression<String>("orderRestImage")
    let orderDate = Expression<String>("orderDate")
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        title = "Menu"
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d-MMM-y"
        self.orderDateString = dateFormatter.string(from: date)
        
        self.cartCheckoutButton.clipsToBounds = true
        self.cartCheckoutButton.layer.cornerRadius = 10
        tableView.delegate = self
        tableView.dataSource = self
        labelRestName.text = self.restaurantName
        let s = NSString(format: "%.1f", self.restaurantRating!)
        labelRestRating.text = "\u{2606} " + (s as String)
        
        do{
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileURL = documentDirectory.appendingPathComponent("orders").appendingPathExtension("sqlite3")
            let db = try Connection(fileURL.path)
            self.orderDatabase = db
            
            let createOrdersTable = self.ordersTable.create { (table) in
                table.column(self.orderID, primaryKey: .autoincrement)
                table.column(self.orderRestName)
                table.column(self.orderRestImage)
                table.column(self.orderRestLocation)
                table.column(self.orderDate)
            }
            do{
//                try self.orderDatabase.run(self.ordersTable.drop(ifExists: true))
                try self.orderDatabase.run(createOrdersTable)
                print("created orders table")
                
            }catch{
                print(error)
            }
            let insertOrder = self.ordersTable.insert(self.orderRestName <- self.restaurantName, self.orderRestImage <- self.restaurantImage, self.orderRestLocation <- self.restaurantLocation, self.orderDate <- self.orderDateString)
            do {
                try self.orderDatabase.run(insertOrder)
                print("inserted order")
            } catch  {
                print(error)
            }
            do{
                let ords = try self.orderDatabase.prepare(self.ordersTable)
                for ord in ords {
                    print("orderID: \(ord[self.orderID]) ordRestName: \(ord[self.orderRestName]) ordRestImage: \(ord[self.orderRestImage])  ordRestLocation: \(ord[self.orderRestLocation])  \(ord[self.orderDate])")
                    orderIDForCheckout = ord[self.orderID]
                }
                
            }catch{
                print(error)
            }
        }catch{
            print(error)
        }
        
        
    }
    
    @IBAction func goToCheckout(_ sender: Any) {
        if(self.clickedMenuItems.count<=0){
            self.view.showToast(toastMessage: "Please add an item before clicking on checkout", duration: 2.0)
        }else{
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CheckoutView") as? CheckoutViewController
            vc?.clickedMenuItems = self.clickedMenuItems
            vc?.orderIDFromMenu = self.orderIDForCheckout
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollDiff = (scrollView.contentOffset.y - previousScrollOffset)
        let isScrollingDown = scrollDiff > 0
        let isScrollingUp = scrollDiff < 0
        if canAnimateHeader(scrollView) {
            var newHeight = headerViewHeight.constant
            if isScrollingDown {
                newHeight = max(minHeaderHeight, headerViewHeight.constant - abs(scrollDiff))
            } else if isScrollingUp {
                newHeight = min(maxHeaderHeight, headerViewHeight.constant + abs(scrollDiff))
            }
            if newHeight != headerViewHeight.constant {
                headerViewHeight.constant = newHeight
                setScrollPosition()
                previousScrollOffset = scrollView.contentOffset.y
            }
        }
    }
    func canAnimateHeader (_ scrollView: UIScrollView) -> Bool {
        let scrollViewMaxHeight = scrollView.frame.height + self.headerViewHeight.constant - minHeaderHeight
        return scrollView.contentSize.height > scrollViewMaxHeight
    }
    
    func setScrollPosition() {
        self.tableView.contentOffset = CGPoint(x:0, y: 0)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! RestaurantMenuCell
        cell.configureItem(menuItem:menuItems[indexPath.row])
        cell.selectionStyle = .none
        cell.cellDelegate = self
        cell.indexPath = indexPath
        cell.menuItemAddButton.tag = indexPath.row
        
        return cell
    }
    
    func didPressAddButton(indexPath: IndexPath) {
        guard let cell = self.tableView.cellForRow(at: indexPath) as? RestaurantMenuCell else {
                return
            }
        let doesExist = self.clickedMenuItems.contains { $0.name == cell.menuItem!.name }
        print(doesExist)
        if(doesExist){
            var quant = self.clickedMenuItems.filter {$0.name == cell.menuItem!.name}.first!.quantity
            quant = quant+1
            self.clickedMenuItems.filter {$0.name == cell.menuItem?.name}.first!.quantity = quant
            if(cell.menuItemAddButton.tag == indexPath.row){
                cell.menuItemAddButton.setTitle("\(quant)", for: .normal)
            }
        }else{
            var quant = cell.menuItem!.quantity
            quant = quant+1
            cell.menuItem?.quantity = quant
            self.clickedMenuItems.append(cell.menuItem!)
            if(cell.menuItemAddButton.tag == indexPath.row){
                cell.menuItemAddButton.setTitle("\(quant)", for: .normal)
            }
        }
        if(self.clickedMenuItems.count>0){
            self.cartCheckoutButton.isHidden = false
            self.cartCheckoutButton.setTitle("Cart Size : \(self.clickedMenuItems.count)", for: .normal)
        }
        
        
        for item in self.clickedMenuItems {
            print("itemName: \(item.name) itemPrice: \(item.price) itemImage: \(item.image) itemQuant: \(item.quantity) ")
        }
    }
    
}
