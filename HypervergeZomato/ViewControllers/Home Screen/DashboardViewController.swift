//
//  DashboardViewController.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 18/02/21.
//

import UIKit
import CoreImage
import FirebaseFirestore

class DashboardViewController: UIViewController {
    var db = Firestore.firestore()
    var userPanNo: String?
    var userName: String?
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var ivProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPANNo: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var progress: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        title = "Profile"
        cardView.isHidden = true
        progress.isHidden = false
        progress.startAnimating()
        
        let defaults = UserDefaults.standard
        if let stringOne = defaults.string(forKey: "panNo") {
            print(stringOne) // Some String Value
            self.userPanNo = stringOne
        }
        if let stringTwo = defaults.string(forKey: "panName") {
            print(stringTwo) // Another String Value
            self.userName = stringTwo
        }
        
        /*let imageName = "dp.jpg"
         let image = UIImage(named: imageName)
         
         profileImage.image = image
         logoutButton.clipsToBounds = true
         logoutButton.layer.cornerRadius = 20
         
         if let inputImage = profileImage.image {
         let ciImage = CIImage(cgImage: inputImage.cgImage!)
         
         let options = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
         let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: options)!
         
         let faces = faceDetector.features(in: ciImage)
         
         if let face = faces.first as? CIFaceFeature {
         
         print("Found face at \(face.bounds)")
         
         if face.hasLeftEyePosition {
         print("Found left eye at \(face.leftEyePosition)")
         }
         
         if face.hasRightEyePosition {
         print("Found right eye at \(face.rightEyePosition)")
         }
         
         if face.hasMouthPosition {
         print("Found mouth at \(face.mouthPosition)")
         }
         }
         else{
         debugPrint("No faces detected")
         }
         }*/
        
        ivProfile.clipsToBounds=true
        ivProfile.layer.cornerRadius = 20
        cardView.clipsToBounds = true
        cardView.layer.shadowColor = UIColor.gray.cgColor
        cardView.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        cardView.layer.shadowOpacity = 1.0
        cardView.layer.masksToBounds = false
        cardView.layer.cornerRadius = 20
        
        logoutButton.clipsToBounds = true
        logoutButton.layer.cornerRadius = 10
        
        labelName.text = self.userName
        labelPANNo.text = self.userPanNo
        
        let docRef = db.collection("RegisteredUsers").document("\(self.userPanNo! as String)")
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let imageURLString = document.get("Image") as! String
                
                let myURL = URL(string: imageURLString)
                if let data = try? Data(contentsOf: myURL!)
                {
                    self.ivProfile.image = UIImage(data: data)!
                }
                
            } else {
                print("Document does not exist")
            }
            self.progress.stopAnimating()
            self.progress.isHidden = true
            self.cardView.isHidden = false
            self.cardView.transform = CGAffineTransform(translationX: 0, y: self.view.bounds.size.height)
            UIView.animate(withDuration: 1.0, delay: 0.5, options: .curveEaseIn, animations: {
                
                self.cardView.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            
        }
    }
    
    @IBAction func onLogoutTap(_ sender: Any) {
        let defaults = UserDefaults.standard
        defaults.set("no", forKey: "loggedIn")
        
        self.tabBarController?.view.removeFromSuperview()
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WelcomeView") as? ViewController
        let aObjNavi = UINavigationController(rootViewController: vc!)
        //        self.navigationController?.pushViewController(aObjNavi, animated: true)
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as? AppDelegate)!
        appDelegate.window?.rootViewController = aObjNavi
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
