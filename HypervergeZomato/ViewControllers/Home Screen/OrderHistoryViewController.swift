//
//  OrderHistoryTableViewController.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 18/02/21.
//

import UIKit
import SQLite
class OrderHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var userPanNo: String?
    var userName: String?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noOrdersImageView: UIImageView!
    
    var itemDatabase: Connection!
    let itemsTable = Table("items")
    let itemID = Expression<Int>("itemID")
    let itemName = Expression<String>("itemName")
    let itemPrice = Expression<String>("itemPrice")
    let itemImage = Expression<String>("itemImage")
    let itemQuantity = Expression<Int>("itemQuantity")
    let itemOrderID = Expression<Int>("itemOrderID")
    
    var orderDatabase: Connection!
    let ordersTable = Table("orders")
    let orderID = Expression<Int>("orderID")
    let orderRestName = Expression<String>("orderRestName")
    let orderRestLocation = Expression<String>("orderRestLocation")
    let orderRestImage = Expression<String>("orderRestImage")
    let orderDate = Expression<String>("orderDate")
    
    var emptyOrderIDs: [Int] = []
    var orderDetails: [OrderHistoryCard] = []
    
    var isAnyOrderPresent : Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("Inside view will appear")
        orderDetails = []
        emptyOrderIDs = []
        viewDidLoad()
        guard let navigationController = self.navigationController else { return }
        var navigationArray = navigationController.viewControllers // To get all UIViewController stack as Array
        let temp = navigationArray.last
        navigationArray.removeAll()
        navigationArray.append(temp!) //To remove all previous UIViewController except the last one
        self.navigationController?.viewControllers = navigationArray
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title="Order History"
        noOrdersImageView.isHidden = true
        self.tableView.reloadData()
        
        let defaults = UserDefaults.standard
        if let stringOne = defaults.string(forKey: "panNo") {
            print(stringOne) // Some String Value
            self.userPanNo = stringOne
        }
        if let stringTwo = defaults.string(forKey: "panName") {
            print(stringTwo) // Another String Value
            self.userName = stringTwo
        }
        title = "Order History"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "deleteOrders.svg"), style: .plain,  target: self, action: #selector(deleteOrderHistory))
        
        //OrderDatabase
        do{
            let orderdocumentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let orderfileURL = orderdocumentDirectory.appendingPathComponent("orders").appendingPathExtension("sqlite3")
            let orderdb = try Connection(orderfileURL.path)
            self.orderDatabase = orderdb
            
            let itemdocumentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let itemfileURL = itemdocumentDirectory.appendingPathComponent("items").appendingPathExtension("sqlite3")
            let itemdb = try Connection(itemfileURL.path)
            self.itemDatabase = itemdb
            
            do{
                let ords = try self.orderDatabase.prepare(self.ordersTable)
                for ord in ords {
                    print("Inside orderDatabase Order History")
                    //ItemDatabase
                    let itemsTableForOrderID = self.itemsTable.filter(self.itemOrderID == ord[self.orderID])
                    let its = try self.itemDatabase.prepare(itemsTableForOrderID)
                    var thisOrderItems: [MenuItem] = []
                    for it in its {
                        isAnyOrderPresent = true
                        thisOrderItems.append(MenuItem(name: it[self.itemName], price: (it[self.itemPrice] as NSString).floatValue, image: it[self.itemImage], quantity: it[self.itemQuantity]))
                    }
                    if(thisOrderItems.count == 0){
                        self.emptyOrderIDs.append(ord[self.orderID])
                    }
                    else{
                        self.orderDetails.append(OrderHistoryCard(orderID: ord[self.orderID], orderRestName: ord[self.orderRestName], orderRestLocation: ord[self.orderRestLocation], orderDate: ord[self.orderDate], orderItems: thisOrderItems, orderRestImage: ord[self.orderRestImage] ))
                    }
                    
                }
                print(self.orderDetails.count)
               
            }catch{
                print(error)
            }
        }catch{
            print(error)
        }
        
        if(isAnyOrderPresent){
            DispatchQueue.main.async {
                self.orderDetails.reverse()
                self.tableView.reloadData()
                let cells = self.tableView.visibleCells
                let tableHeight: CGFloat = self.tableView.bounds.size.height
                
                for i in cells {
                    let cell: OrderCell = i as! OrderCell
                    cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
                }
                
                var index = 0
                
                for a in cells {
                    let cell: OrderCell = a as! OrderCell
                    UIView.animate(withDuration: 2.0, delay: 0.1 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
                        cell.transform = CGAffineTransform(translationX: 0, y: 0);
                    }, completion: nil)
                    
                    index += 1
                }
            }
            for ordID in self.emptyOrderIDs{
                let ord = self.ordersTable.filter(self.orderID == ordID)
                let deleteOrd = ord.delete()
                do{
                    try self.orderDatabase.run(deleteOrd)
                }catch{
                    print(error)
                }
            }
        }
        else{
            print("No orders")
            noOrdersInOrderHistory()
        }
        
        
    }
    
    @objc func deleteOrderHistory(){
        if(isAnyOrderPresent){
            let alert = UIAlertController(title: "Alert", message: "Are you sure you want to delete order history", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    do{try self.itemDatabase.run(self.itemsTable.drop(ifExists: true))}catch{print(error)}
                    do{try self.orderDatabase.run(self.ordersTable.drop(ifExists: true))}catch{print(error)}
                    self.isAnyOrderPresent = false
                    self.viewWillAppear(true)
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                @unknown default:
                    print("unknown default")
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "No order to delete", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                @unknown default:
                    print("unknown default")
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func noOrdersInOrderHistory(){
        noOrdersImageView.isHidden = false
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! OrderCell
        cell.configure(orderDetails: self.orderDetails[indexPath.row])
        return cell
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
