//
//  PaymentSuccessfulViewController.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 21/02/21.
//

import UIKit
import Lottie
class PaymentSuccessfulViewController: UIViewController {
    
    @IBOutlet weak var animationView: AnimationView!
    @IBOutlet weak var goToHomeScreen: UIButton!
    @IBOutlet weak var goToOrderHistory: UIButton!
    @IBOutlet weak var paymentSuccLabel: UILabel!
    @IBOutlet weak var orderOnWayLabel: UILabel!
    @IBOutlet weak var receiptLabel: UILabel!
    @IBOutlet weak var receiptOptionStackView: UIStackView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let navigationController = self.navigationController else { return }
        var navigationArray = navigationController.viewControllers // To get all UIViewController stack as Array
        let temp = navigationArray.last
        navigationArray.removeAll()
        navigationArray.append(temp!) //To remove all previous UIViewController except the last one
        self.navigationController?.viewControllers = navigationArray
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Payment"
        
        
        // Do any additional setup after loading the view.
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .playOnce
        animationView.animationSpeed = 2.0
        animationView.play()
        
        goToHomeScreen.clipsToBounds = true
        goToHomeScreen.layer.cornerRadius = 10
        
        goToOrderHistory.clipsToBounds = true
        goToOrderHistory.layer.cornerRadius = 10
        
        //        let oowbounds = self.orderOnWayLabel!.bounds
        //        UIView.animate(withDuration: 1.0, delay: 1.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: .curveEaseIn, animations: {
        //            self.orderOnWayLabel!.bounds = CGRect(x: oowbounds.origin.x - 20, y: oowbounds.origin.y, width: oowbounds.size.width + 60, height: oowbounds.size.height)
        //            self.view.layoutIfNeeded()
        //        }, completion: nil)
        //
        //        let rlbounds = self.receiptLabel!.bounds
        //        UIView.animate(withDuration: 1.0, delay: 2.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: .curveEaseIn, animations: {
        //            self.receiptLabel!.bounds = CGRect(x: rlbounds.origin.x - 20, y: rlbounds.origin.y, width: rlbounds.size.width + 60, height: rlbounds.size.height)
        //            self.view.layoutIfNeeded()
        //        }, completion: nil)
        //
        //        let rosvbounds = self.receiptOptionStackView!.bounds
        //        UIView.animate(withDuration: 1.0, delay: 3.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: .curveEaseIn, animations: {
        //            self.receiptOptionStackView!.bounds = CGRect(x: rosvbounds.origin.x - 20, y: rosvbounds.origin.y, width: rosvbounds.size.width + 60, height: rosvbounds.size.height)
        //            self.view.layoutIfNeeded()
        //        }, completion: nil)
        
        
    }
    
    
    @IBAction func goToOrderHIstoryView(_ sender: Any) {
        
        /*let pslbounds = self.goToOrderHistory!.bounds
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: .curveEaseIn, animations: {
            self.goToOrderHistory!.bounds = CGRect(x: pslbounds.origin.x - 20, y: pslbounds.origin.y, width: pslbounds.size.width + 60, height: pslbounds.size.height)
//            self.view.layoutIfNeeded()
        }, completion: {_ in
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderHistoryFrag") as? OrderHistoryViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        })*/
//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderHistoryFrag") as? OrderHistoryViewController
//        self.navigationController?.pushViewController(vc!, animated: true)
//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderHistoryFrag") as? OrderHistoryViewController
//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeFrag") as? HomeViewController
//        self.navigationController?.pushViewController(vc!, animated: true)
        self.tabBarController?.selectedIndex = 2
    }
    @IBAction func goToHomeScreenView(_ sender: Any) {
        /*let pslbounds = self.goToOrderHistory!.bounds
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: .curveEaseIn, animations: {
            self.goToOrderHistory!.bounds = CGRect(x: pslbounds.origin.x - 20, y: pslbounds.origin.y, width: pslbounds.size.width + 60, height: pslbounds.size.height)
//            self.view.layoutIfNeeded()
        }, completion: {_ in
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeFrag") as? HomeViewController
//            vc?.userPanNo = self.panNo
//            vc?.userName = self.userName
            self.navigationController?.pushViewController(vc!, animated: true)
        })*/
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeFrag") as? HomeViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
