//
//  TryViewController.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 17/02/21.
//

import UIKit

class RegisterPANCameraViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate{
    
    var imagePicker: UIImagePickerController!
    
    @IBOutlet var proceedPAN: UIButton!
    @IBOutlet var ivPAN: UIImageView!
    @IBOutlet var capturePAN: UIButton!
    @IBOutlet var progressPAN: UIActivityIndicatorView!
    
    
    var panImage: UIImage?
    var userName: String?
    var panNo: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        title="Register - Capture PAN"
        
        ivPAN.clipsToBounds = true
        ivPAN.layer.cornerRadius = 8
        
        capturePAN.clipsToBounds = true
        capturePAN.layer.cornerRadius = 10
        
        proceedPAN.clipsToBounds = true
        proceedPAN.layer.cornerRadius = 10
        
        self.progressPAN.isHidden = true
        self.proceedPAN.isHidden = true
        
        self.view.showToast(toastMessage: "Capture PAN Image by clicking on capture button", duration: 2.0)
        
        
    }
    
    
    @IBAction func onCaptureTap(){
        self.progressPAN.isHidden = false
        self.progressPAN.startAnimating()
        self.capturePAN.isHidden = true
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true

        present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
        self.progressPAN.isHidden = true
        self.progressPAN.stopAnimating()
        self.capturePAN.isHidden = false
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        panImage = info[.editedImage] as? UIImage
        ivPAN.image = panImage
        title = "Register - Review PAN"
        self.progressPAN.isHidden = true
        self.progressPAN.stopAnimating()
        self.proceedPAN.isHidden = false
    }
    
    @IBAction func onProceedTap(){
        DispatchQueue.main.async {
            self.progressPAN.startAnimating()
            self.progressPAN.isHidden = false
            self.proceedPAN.isHidden = true
        }
        
        // make url
        let boundary = "Boundary-\(UUID().uuidString)"
        var request = URLRequest(url: URL(string: "https://ind-docs.hyperverge.co/v2.0/readKYC")!,timeoutInterval: Double.infinity)
        request.addValue("857acb", forHTTPHeaderField: "appId")
        request.addValue("e0d888e703691bccbf92", forHTTPHeaderField: "appKey")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let imageData = ivPAN.image!.jpegData(compressionQuality: 0.7)! as NSData
        
        request.httpMethod = "POST"
        request.httpBody = createBodyWithParameters(filePathKey: "file", imageDataKey: imageData, boundary: boundary) as Data
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let dataResponse = data else {
                print(String(describing: error))
                return
            }
            print(String(data: data!, encoding: .utf8)!)
            do{
                //here dataResponse received from a network request
                let jsonResponse = try JSONSerialization.jsonObject(with:
                                                                        dataResponse, options: [])
                guard let jsonObject = jsonResponse as? [String: Any] else {
                    return
                }
                guard let result = jsonObject["result"] as? [[String:Any]] else { return }
                guard let details = result[0]["details"] as? [String: Any] else {return}
                guard let name = details["name"] as? [String: Any] else{return}
                guard let pan = details["pan_no"] as? [String: Any] else{return}
                
                self.userName = name["value"] as? String
                self.panNo = pan["value"] as? String
                
            } catch let parsingError {
                print("Error", parsingError)
            }
            DispatchQueue.main.async {
                self.progressPAN.stopAnimating()
                self.progressPAN.isHidden = true
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main)
                    .instantiateViewController(withIdentifier: "RegisterSelfieCameraView") as? RegisterSelfieCameraViewController;
                vc?.panImage = self.panImage
                vc?.userName = self.userName
                vc?.panNo = self.panNo
                self.navigationController?.pushViewController(vc!, animated: true)
                
                self.title="Register - Capture PAN"
                self.proceedPAN.isHidden = true
                self.capturePAN.isHidden = false
                self.progressPAN.isHidden = true
                self.ivPAN.image = UIImage(named: "dummy pan card.jpg")
            }
        }
        task.resume()
    }
    
    func createBodyWithParameters(filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData{
        let body = NSMutableData();
        let key = "image"
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(key)\"")
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString("\r\n")
        body.appendString("--\(boundary)--\r\n")
        return body
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
