//
//  RegisterSelfieCameraViewController.swift
//  HypervergeZomato
//
//  Created by Shrirama Upadhya A on 19/02/21.
//

import UIKit
import FirebaseStorage
import FirebaseFirestore
class RegisterSelfieCameraViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    var storage = Storage.storage()
    var db = Firestore.firestore()
    
    var imagePicker: UIImagePickerController!
    @IBOutlet var ivSelfie: UIImageView!
    @IBOutlet var captureSelfie: UIButton!
    @IBOutlet var progressSelfie: UIActivityIndicatorView!
    @IBOutlet var proceedSelfie: UIButton!
    
    var panImage: UIImage?
    var selfieImage: UIImage?
    var panNo: String?
    var userName: String?
    var isMatch: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Register - Capture Selfie"
        
        ivSelfie.clipsToBounds = true
        ivSelfie.layer.cornerRadius = 8
        
        captureSelfie.clipsToBounds = true
        captureSelfie.layer.cornerRadius = 10
        
        proceedSelfie.clipsToBounds = true
        proceedSelfie.layer.cornerRadius = 10
        
        self.progressSelfie.isHidden = true
        self.proceedSelfie.isHidden = true
        
        self.view.showToast(toastMessage: "Capture Selfie by clicking on capture button", duration: 2.0)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func onCaptureSelfieTap(_ sender: Any) {
        self.progressSelfie.isHidden = false
        self.progressSelfie.startAnimating()
        self.captureSelfie.isHidden = true
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
//        imagePicker.allowsEditing = true

        
        present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
        self.progressSelfie.isHidden = true
        self.progressSelfie.stopAnimating()
        self.captureSelfie.isHidden = false
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        let img = info[.originalImage] as? UIImage
        self.selfieImage = info[.originalImage] as? UIImage
        ivSelfie.image = UIImage(cgImage: (img?.cgImage)!, scale: 1.0, orientation: .downMirrored)
        ivSelfie.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
        //        ivSelfie.image = info[.originalImage] as? UIImage
        
        title = "Register - Review Selfie"
        self.progressSelfie.isHidden = true
        self.progressSelfie.stopAnimating()
        self.proceedSelfie.isHidden = false
    }
    @IBAction func onProceedTap(){
        
        DispatchQueue.main.async {
            self.progressSelfie.isHidden = false
            self.progressSelfie.startAnimating()
            self.proceedSelfie.isHidden = true
        }
        let boundary = "Boundary-\(UUID().uuidString)"
        var request = URLRequest(url: URL(string: "https://ind-faceid.hyperverge.co/v1/photo/verifyPair")!,timeoutInterval: Double.infinity)
        request.addValue("857acb", forHTTPHeaderField: "appId")
        request.addValue("e0d888e703691bccbf92", forHTTPHeaderField: "appKey")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let panImageData = panImage!.jpegData(compressionQuality: 0.7)! as NSData
        let selfieImageData = selfieImage!.jpegData(compressionQuality: 0.7)! as NSData
        
        let imageDataDict = ["selfie" : panImageData, "id" : selfieImageData]
        
        request.httpMethod = "POST"
        request.httpBody = createBodyWithParameters(filePathKey: "file", imageDataKey: imageDataDict, boundary: boundary) as Data
        
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let dataResponse = data else {
                print(String(describing: error))
                return
            }
            do{
                //here dataResponse received from a network request
                let jsonResponse = try JSONSerialization.jsonObject(with:
                                                                        dataResponse, options: [])
                guard let jsonObject = jsonResponse as? [String: Any] else {
                    return
                }
                guard let result = jsonObject["result"] as? [String:Any] else { return }
                print(result)
                if(result["match"] as! String == "yes"){
                    self.isMatch=true
                    let storageRef = self.storage.reference()
                    // Data in memory
                    let data = selfieImageData as Data
                    let panString = self.panNo! as String
                    let panNameString = self.userName! as String
                    let riversRef = storageRef.child("Images/\(panString)")
                    let uploadTask = riversRef.putData(data, metadata: nil) { (metadata, error) in
                      guard let metadata = metadata else {
                        // Uh-oh, an error occurred!
                        return
                      }
                      // Metadata contains file metadata such as size, content-type.
                      let size = metadata.size
                      // You can also access to download URL after upload.
                      riversRef.downloadURL { (url, error) in
                        guard let downloadURL = url else {
                          // Uh-oh, an error occurred!
                          return
                        }
                        self.db.collection("RegisteredUsers").document("\(panString)").setData([
                            "Name": "\(panNameString)",
                            "Image": "\(downloadURL)",
                            "ID": "\(panString)"
                        ]) { err in
                            if let err = err {
                                print("Error writing document: \(err)")
                            } else {
                                
                                let defaults = UserDefaults.standard
                                defaults.set("\(panString)", forKey: "panNo")
                                defaults.set("\(panNameString)", forKey: "panName")
                                defaults.set("yes", forKey: "loggedIn")
                                
                                self.view.showToast(toastMessage: "Registration Successful", duration: 1.0)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5)  {
                                    self.title = "Register - Capture Selfie"
                                    self.progressSelfie.isHidden = true
                                    self.progressSelfie.stopAnimating()
                                    self.proceedSelfie.isHidden = true
                                    self.captureSelfie.isHidden = false
                                    self.ivSelfie.image = UIImage(named: "dummy selfie.jpg")
                                    self.ivSelfie.transform = .identity

                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabView") as? TabViewController
                                    vc?.userPanNo = self.panNo
                                    vc?.userName = self.userName
                                    self.navigationController?.pushViewController(vc!, animated: true)
                                }
                            }
                        }
                      }
                    }
                    
                    
                }else{
                    self.isMatch=false
                    DispatchQueue.main.async {
                        self.view.showToast(toastMessage: "PAN Card Image and Selfie do not match, Please try again", duration: 2.0)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                            // your code here
                            self.title = "Register - Capture Selfie"
                            self.progressSelfie.isHidden = true
                            self.progressSelfie.stopAnimating()
                            self.proceedSelfie.isHidden = true
                            self.captureSelfie.isHidden = false
                            self.ivSelfie.image = UIImage(named: "dummy selfie.jpg")
                            self.ivSelfie.transform = .identity
                            //                            guard let navigationController = self.navigationController else { return }
                            //                            var navigationArray = navigationController.viewControllers // To get all UIViewController stack as Array
                            //                            //                            navigationArray.remove(at: navigationArray.count - 2)
                            //                            navigationArray.remove(at: navigationArray.count - 1)
                            //                            self.navigationController?.viewControllers = navigationArray
                            //                            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main)
                            //                                .instantiateViewController(withIdentifier: "RegisterSelfieCameraView") as? RegisterSelfieCameraViewController;
                            //                            vc?.panImage = self.panImage
                            //                            self.navigationController?.pushViewController(vc!, animated: true)
                        }
                        
                    }
                    
                    
                    
                }
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        
        task.resume()
        
        
    }
    func createBodyWithParameters(filePathKey: String?, imageDataKey: [String : NSData], boundary: String) -> NSData{
        let body = NSMutableData();
        
        for(k, value) in imageDataKey{
            let key = k
            let filename = "\(key).jpg"
            let mimetype = "image/jpg"
            
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
            body.appendString("Content-Type: \(mimetype)\r\n\r\n")
            body.append(value as Data)
            body.appendString("\r\n")
            
        }
        body.appendString("--\(boundary)--\r\n")
        
        return body
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
